#include "pch.h"
#include <iostream>
#include <regex>

const unsigned int
                  DIMENSIONS_NUMBER = 3, // You should only modify the DIMENSIONS value in the source code
 FIRST_POINT_FIRST_COORDINATE_INDEX = 1,
  FIRST_POINT_LAST_COORDINATE_INDEX = FIRST_POINT_FIRST_COORDINATE_INDEX + DIMENSIONS_NUMBER - 1,
                 FIRST_RADIUS_INDEX = FIRST_POINT_LAST_COORDINATE_INDEX + 1,
SECOND_POINT_FIRST_COORDINATE_INDEX = FIRST_RADIUS_INDEX + 1,
 SECOND_POINT_LAST_COORDINATE_INDEX = SECOND_POINT_FIRST_COORDINATE_INDEX + DIMENSIONS_NUMBER - 1,
                SECOND_RADIUS_INDEX = SECOND_POINT_LAST_COORDINATE_INDEX + 1;

const unsigned long long int
REQUIRED_PARAMETER_COUNT = 1 + (((const unsigned long long int)DIMENSIONS_NUMBER) + 1) * 2;

const float
EPSILON = 0.01f; // This value is used for more precise intersection examination

const std::string
DIGIT_FORMAT("(-|\\+)?(0|[1-9][0-9]*)([.][0-9])?"); // Fraction number with maximum 1 decimal place

const std::regex
DIGIT_REGEX(DIGIT_FORMAT);

enum class
IntersectionType { NO_INTERSECTION, SINGLE_POINT_INTERSECTION, MULTI_POINT_INTERSECTION };

void
ZeroDimensionHandler              (),
ParameterCountCheck               (const unsigned int argumentCount),
ParameterTypeCheck                (const unsigned int argumentCount, const char** arguments),
FillPointVector                   (const char** arguments, std::vector<float> &point,
                                   const unsigned int startIndex, const unsigned int endIndex);
IntersectionType
RelationOfTwoHyperSpheres (const std::vector<float> &point1, const float radius1,
                                   const std::vector<float> &point2, const float radius2);
float
CalculateDistanceOfTwoPoints      (const std::vector<float> &point1,
                                   const std::vector<float> &point2);

int main(const unsigned int argc, const char** argv)
{
  if (DIMENSIONS_NUMBER == 0)
  {
    ZeroDimensionHandler();
  }

  ParameterCountCheck(argc);
  ParameterTypeCheck(argc, argv);

  std::vector<float>
  point1 = std::vector<float>(DIMENSIONS_NUMBER),
  point2 = std::vector<float>(DIMENSIONS_NUMBER);

  const float
  radius1 = std::stof(argv[FIRST_RADIUS_INDEX]),
  radius2 = std::stof(argv[SECOND_RADIUS_INDEX]);

  FillPointVector(argv, point1, FIRST_POINT_FIRST_COORDINATE_INDEX, FIRST_POINT_LAST_COORDINATE_INDEX);
  FillPointVector(argv, point2, SECOND_POINT_FIRST_COORDINATE_INDEX, SECOND_POINT_LAST_COORDINATE_INDEX);

  for (const auto value : point1)
  {
    std::cout << value << ", ";
  }
  std::cout << std::endl << radius1 << std::endl << std::endl;

  for (const auto value : point2)
  {
    std::cout << value << ", ";
  }
  std::cout << std::endl << radius2 << std::endl;

  IntersectionType intersectionType = RelationOfTwoHyperSpheres(point1, radius1, point2, radius2);

  if (intersectionType == IntersectionType::NO_INTERSECTION)
  {
    std::cout << "The 2 shapes have no intersection points!" << std::endl;
    return 0;
  }
  else if (intersectionType == IntersectionType::SINGLE_POINT_INTERSECTION)
  {
    std::cout << "The 2 shapes have 1 intersection point !" << std::endl;
    // do some math stuff here to get the coordinates of that point
  }
  else
  {
    // also do some math stuff here
    // to get the equation of the n-dim shape that is described by the intersection
    // of the 2 original n-dim shapes
  }
  return 0;
}

void
ZeroDimensionHandler()
{
  std::cout << "Current DIMENSION is 0!" << std::endl
            << "There is no such thing like intersection in zero dimension!" << std::endl;
  exit(0);
}

void ParameterCountCheck 
(const unsigned int argumentCount)
{
  if (argumentCount != REQUIRED_PARAMETER_COUNT)
  {
    std::cout << "ERRORCODE(1): Wrong parameter count!" << std::endl << std::endl
              << "Current parameter count: " << argumentCount << std::endl
              << "Required parameter count: " << REQUIRED_PARAMETER_COUNT << std::endl << std::endl
              << "Furthermore, if Requiered parameter count value is too large, "
              << "maybe the corresponding variable (REQUIRED_PARAMETER_COUNT) was overflowed!"
              << std::endl << std::endl;
    exit(1);
  }
}

void ParameterTypeCheck
(const unsigned int argumentCount, const char** arguments)
{
  std::vector<std::string> wrongParameters(0);
  for (unsigned int i = 1; i < argumentCount; ++i)
  {
    if (!std::regex_match(arguments[i], DIGIT_REGEX) ||
      ((i == FIRST_RADIUS_INDEX || i == SECOND_RADIUS_INDEX) && std::stof(arguments[i]) < 0))
    {
      wrongParameters.push_back(arguments[i]);
    }
  }

  if (wrongParameters.size() > 0)
  {
    std::cout << "ERRORCODE(2): The format of the below listed parameters are not appropiate:" << std::endl;
    for (auto& parameter : wrongParameters)
    {
      std::cout << "-> " << parameter << std::endl << std::endl;
    }
    std::cout << "Acceptable format: Fraction number with maximum 1 decimal place(" << DIGIT_FORMAT << ")" << std::endl
              << "Futhermore, the radius parameters must be Non-Negative" << std::endl << std::endl;
    exit(2);
  }
}

void FillPointVector
(const char** arguments, std::vector<float> &point,
 const unsigned int startIndex, const unsigned int endIndex)
{
  unsigned int
  currentIndex = startIndex,
  vectorIndex = 0;
  while (currentIndex <= endIndex)
  {
    point[vectorIndex] = std::stof(arguments[currentIndex]);
    ++currentIndex; ++vectorIndex;
  }
}

IntersectionType RelationOfTwoHyperSpheres
(const std::vector<float> &point1, const float radius1,
 const std::vector<float> &point2, const float radius2)
{
  float
  sumOfRadiuses    = radius1 + radius2,
  distanceOfPoints = CalculateDistanceOfTwoPoints(point1, point2);

  std::string dbgMsg;

  IntersectionType intersectionType;
  if (std::abs(distanceOfPoints - sumOfRadiuses) < EPSILON)
  {
    intersectionType = IntersectionType::SINGLE_POINT_INTERSECTION;
    dbgMsg = "SINGLE_POINT_INTERSECTION";
  }
  else if (distanceOfPoints > sumOfRadiuses)
  {
    intersectionType = IntersectionType::NO_INTERSECTION;
    dbgMsg = "NO_INTERSECTION";
  }
  else if (distanceOfPoints < sumOfRadiuses)
  {
    intersectionType = IntersectionType::MULTI_POINT_INTERSECTION;
    dbgMsg = "MULTI_POINT_INTERSECTION";
  }
  
  std::cout << dbgMsg << "\nDistance of points: " << distanceOfPoints << std::endl;

  return intersectionType;
}

float CalculateDistanceOfTwoPoints
(const std::vector<float> &point1,
 const std::vector<float> &point2)
{
  float squareDistanceOfPoints = 0;
  for (unsigned int i = 0; i < DIMENSIONS_NUMBER; ++i)
  {
    squareDistanceOfPoints += powf(point1[i] - point2[i], 2);
  }
  return sqrtf(squareDistanceOfPoints);
}