@echo off

x64\Debug\Intersection.exe %= Program to be run =%^
            1.1 1 1        %= Coordinates of FIRST center point =%^
              +2.1         %= Radius of FIRST shape =%^
            3.4 1 1        %= Coordinates of SECOND center point =%^
              0.2          %= Radius of SECOND shape =%

pause