I. PROBLEM STATEMENT

We want to perform intersection detection between two hyperspheres Sn0 and Sn1,
where n ≥ 0 is the dimensions of the enclosed space (thus S2 is a circle,
S3 is a casual sphere, S4 is a glome, etc.). The result of the intersection
should describe the relation of the hyperspheres and the set of all intersection
points, if any. We must also handle any degenerate and special cases.

II. TASK

Design and implement a reusable function in standard C++, which solves the
intersection detection problem described in the previous section.
During the task you also have to design and implement a data structure for the
result of the intersection detection. The number of dimensions n is given as a 
compile time constant N. The inputs and outputs of the function are:

A. Input
1) the 1st hypersphere Sn0
2) the 2nd hypersphere Sn1
Each hypersphere is represented as an aggregate structure of an N-dimensional
point p and a scalar radius r. The underlying numerical values are represented
by the IEC-559 single-precision binary floating-point format.

B. Output
1) the result of the intersection, which contains the relation of the
hyperspheres and describes the set of all intersection points

III. MISCELLANEOUS

What will be observed in the soultion:
    -complexity;
    -optimization, speed;
    -handling boundings;
    -coding style;